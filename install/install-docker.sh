#!/bin/bash -ex
# https://docs.docker.com/config/daemon/

if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root" 
   exit 1
fi

# Save input arguments for debug and diagnosis
echo "$@" >> .node

# Set defaults
USER=${1:-"stack"}
#USER=vagrant

# k8s v1.9.1 complains if any later version of docker is used
DCKRV=${2:-"docker-ce=17.03.2~ce-0~ubuntu-xenial"}
#apt-get install -y docker-ce=17.09
#apt-get install -y docker-ce

# there is no 17.03.2
DCKRPKG=${3:-"docker-engine_17.03.1~ce-0~ubuntu-xenial_amd64.deb"}


# Install using dpkg
docker_pkg_install()
{
    DCKRPKGURL="https://apt.dockerproject.org/repo/pool/main/d/docker-engine/${1}"
    curl -sLO $DCKRPKGURL
    dpkg -i ${1}
    rm ${1}
}

# Install using apt-get
docker_install()
{
    apt-get install -y apt-transport-https ca-certificates curl software-properties-common
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | apt-key add -
    add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
    apt-get update -y
    apt-get install -y ${1}
}

docker_add_group()
{
    getent group docker || groupadd docker
    id | grep -q docker || usermod -aG docker ${1}
    #newgrp docker
    docker version
}

docker_customize()
{
    # Nice to have: make sure if dockerd is restarted it won't kill k8s
    cat <<EOF >/etc/docker/daemon.json
{
  "live-restore": true
}
EOF

    # service docker restart
    systemctl restart docker.service
}

docker_install $DCKRV
docker_add_group $USER
docker_customize
docker info

