#!/bin/bash -e
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

echo "$*" >> .node

CNICALICO="http://docs.projectcalico.org/v2.6/getting-started/kubernetes/installation/hosted/kubeadm/1.6/calico.yaml"
CNIFLANNEL="https://raw.githubusercontent.com/coreos/flannel/v0.9.1/Documentation/kube-flannel.yml"
CNIPLUG=${1:-"calico"}
case $CNIPLUG in
    "calico")
	CNIPLUG=$CNICALICO
	;;
    *)
	CNIPLUG=$CNIFLANNEL
	;;
esac

CIDR=${2:-"192.168.0.0/16"}

INTF=${3:-"eth0"}
APIIP=$(ip a show dev $INTF | grep -w inet | awk -e '{print $2}' | cut -d'/' -f1)

USER=${4:-"stack"}

TOK=${5:-"00b851.ec0933704172bb5f"}

#VERSION=v1.7.5
VERSION=${6:-"v1.9.3"}


# Also, I haven't put the iptables forward in yet. With 1.7.5 k8s services were not
# accessible without it. Will wait to see if its still so before putting the hack in.
#
# https://kubernetes.io/docs/reference/setup-tools/kubeadm/kubeadm-init/
# -------------------------------------------------------------
install_k8s_master()
{
    kubeadm init --token $5 --kubernetes-version $1 --apiserver-advertise-address $4 --pod-network-cidr $3
    #TOK=$(kubeadm token list | grep bootstrap | awk '{print $1}')

    rm -rf $HOME/.kube && mkdir -p $HOME/.kube
    cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
    #chown $(id -u):$(id -g) $HOME/.kube/config
    chown -R ${2}:${2} $HOME/.kube
    kubectl apply -f $6
}

install_k8s_node()
{
    # Have to figure out how to get the discovery-token-ca-cert-hash
##    kubeadm join --token $1 ${2}:6443 --discovery-token-ca-cert-hash sha256:489681fc136fbe64180ef6c12f62658b2f7caa107d795d3eb43fd36bbd509095
    kubeadm join --token $1 ${2}:6443 --discovery-token-unsafe-skip-ca-verification
}

install_k8s_master $VERSION $USER $CIDR $APIIP $TOK $CNIPLUG
