#!/bin/bash -e
if [[ $EUID -ne 0 ]]; then
   echo "This script must be run as root"
   exit 1
fi

echo "$*" >> .node

# From: https://packages.cloud.google.com/apt/dists/kubernetes-xenial/main/binary-amd64/Packages
#K8SVER=${1:-"1.7.5-00"}
#K8SVER=${1:-"1.9.1-00"}
K8SVER=${1:-"1.9.3-00"}

# https://kubernetes.io/docs/setup/independent/install-kubeadm/
# -------------------------------------------------------------
install_kubeadm()
{
    apt-get install -y apt-transport-https
    curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
    echo deb http://apt.kubernetes.io/ kubernetes-xenial main | tee /etc/apt/sources.list.d/kubernetes.list
    apt-get update
    apt-get install -y kubelet=$1 kubeadm=$1
    kubeadm version
}

install_kubeadm $K8SVER
