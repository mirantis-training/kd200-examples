https://kubernetes.io/docs/admin/multiple-zones/

SelectorSpreadPriority:
https://github.com/openshift/openshift-ansible/issues/476
https://github.com/openshift/origin/pull/4175
https://github.com/kubernetes/kubernetes/pull/12544
https://github.com/kubernetes/kubernetes/issues/12542
https://github.com/kubernetes/kubernetes/blob/release-1.0/plugin/pkg/scheduler/algorithmprovider/defaults/defaults.go#L70
https://kubernetes.io/docs/reference/labels-annotations-taints/#failure-domainbetakubernetesiozone

addons:
https://github.com/kubernetes/kubernetes/tree/master/cluster/addons
https://github.com/kubernetes/kubernetes/blob/master/cluster/addons/addon-manager/README.md

